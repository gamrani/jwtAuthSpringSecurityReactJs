#### Stage 1: Build the application
FROM openjdk:8-jdk-alpine as build

WORKDIR /app

VOLUME /tmp
ARG JAR_FILE=/target/Api-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar

EXPOSE 5001
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/app.jar"]
