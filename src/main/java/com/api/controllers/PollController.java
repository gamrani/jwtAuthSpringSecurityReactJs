package com.api.controllers;

import com.api.dto.request.PollRequestDTO;
import com.api.dto.response.PagedResponseDTO;
import com.api.dto.response.PollResponseDTO;
import com.api.mappers.PollMapper;
import com.api.models.Poll;
import com.api.security.CurrentUser;
import com.api.security.PollUserPrincipal;
import com.api.services.PollService;
import com.api.type.AppConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/polls")
public class PollController {

    @Autowired
    private PollService pollService;

    private static final Logger logger = LoggerFactory.getLogger(PollController.class);

    @GetMapping
    public PagedResponseDTO<PollResponseDTO> getPolls(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER ) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {

        PollUserPrincipal currentUser = (PollUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return pollService.getAllPolls(currentUser, page, size);
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<Poll> createPoll(@Valid @RequestBody PollRequestDTO pollRequest) {
        Poll poll = pollService.createPoll(pollRequest);

        return new ResponseEntity<>(poll, HttpStatus.CREATED);
    }


}
