package com.api.dto.request;

import javax.validation.constraints.NotNull;

public class VoteRequestDTO {
    @NotNull
    private Long choiceId;

    public Long getChoiceId() {
        return choiceId;
    }

    public void setChoiceId(Long choiceId) {
        this.choiceId = choiceId;
    }
}
