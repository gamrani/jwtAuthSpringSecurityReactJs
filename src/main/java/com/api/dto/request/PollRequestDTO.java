package com.api.dto.request;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class PollRequestDTO {
    @NotBlank
    @Size(max = 140)
    private String question;

    @NotNull
    @Size(min = 2, max = 6)
    @Valid
    private List<ChoiceRequestDTO> choices;

    @NotNull
    @Valid
    private PollLengthDTO pollLength;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<ChoiceRequestDTO> getChoices() {
        return choices;
    }

    public void setChoices(List<ChoiceRequestDTO> choices) {
        this.choices = choices;
    }

    public PollLengthDTO getPollLength() {
        return pollLength;
    }

    public void setPollLength(PollLengthDTO pollLength) {
        this.pollLength = pollLength;
    }
}
