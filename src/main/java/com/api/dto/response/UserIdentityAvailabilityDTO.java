package com.api.dto.response;

public class UserIdentityAvailabilityDTO {
    private Boolean available;

    public UserIdentityAvailabilityDTO(Boolean available) {
        this.available = available;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }
}
